package ru.t1.aayakovlev.tm.exception.auth;

import ru.t1.aayakovlev.tm.exception.AbstractException;

public abstract class AbstractAuthException extends AbstractException {

    public AbstractAuthException() {
    }

    public AbstractAuthException(String message) {
        super(message);
    }

    public AbstractAuthException(String message, Throwable cause) {
        super(message, cause);
    }

    public AbstractAuthException(Throwable cause) {
        super(cause);
    }

    public AbstractAuthException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
