package ru.t1.aayakovlev.tm.exception.system;

public final class ArgumentNotSupportedException extends AbstractSystemException {

    public ArgumentNotSupportedException() {
        super("Error! Passed argument not recognized...");
    }

    public ArgumentNotSupportedException(final String argument) {
        super("Error! Passed argument '" + argument + "' not supported...");
    }

}
