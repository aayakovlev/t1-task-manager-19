package ru.t1.aayakovlev.tm.repository;

import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.field.AbstractFieldException;
import ru.t1.aayakovlev.tm.exception.field.IndexIncorrectException;
import ru.t1.aayakovlev.tm.model.AbstractExtendedModel;

import java.util.Comparator;
import java.util.List;

public interface ExtendedRepository<M extends AbstractExtendedModel> extends BaseRepository<M> {

    int count();

    void deleteAll() throws AbstractException;

    boolean existsById(final String id);

    List<M> findAll(final Comparator<M> comparator);

    M findByIndex(final Integer index) throws IndexIncorrectException;

    M removeByIndex(final Integer index) throws AbstractFieldException;

}
