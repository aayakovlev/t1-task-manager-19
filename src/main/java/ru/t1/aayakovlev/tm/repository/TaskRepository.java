package ru.t1.aayakovlev.tm.repository;

import ru.t1.aayakovlev.tm.model.Task;

import java.util.List;

public interface TaskRepository extends ExtendedRepository<Task> {

    Task create(final String name);

    Task create(final String name, final String description);

    List<Task> findAllByProjectId(final String projectId);

}
