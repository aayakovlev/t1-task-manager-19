package ru.t1.aayakovlev.tm.repository;

import ru.t1.aayakovlev.tm.model.User;

public interface UserRepository extends BaseRepository<User> {

    User findByLogin(final String login);

    User findByEmail(final String email);

    boolean isLoginExists(final String login);

    boolean isEmailExists(final String email);

}
