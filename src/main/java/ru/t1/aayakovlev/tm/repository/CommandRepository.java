package ru.t1.aayakovlev.tm.repository;

import ru.t1.aayakovlev.tm.command.AbstractCommand;

import java.util.Collection;

public interface CommandRepository {

    void add(final AbstractCommand command);

    AbstractCommand getCommandByArgument(final String argument);

    AbstractCommand getCommandByName(final String name);

    Collection<AbstractCommand> getTerminalCommands();

}
