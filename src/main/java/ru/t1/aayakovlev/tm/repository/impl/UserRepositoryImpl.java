package ru.t1.aayakovlev.tm.repository.impl;

import ru.t1.aayakovlev.tm.model.User;
import ru.t1.aayakovlev.tm.repository.UserRepository;


public final class UserRepositoryImpl extends AbstractBaseRepository<User> implements UserRepository {

    @Override
    public User findByLogin(final String login) {
        for (final User user : models) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User findByEmail(final String email) {
        for (final User user : models) {
            if (email.equals(user.getEmail())) return user;
        }
        return null;
    }

    @Override
    public boolean isLoginExists(final String login) {
        for (final User user : models) {
            if (login.equals(user.getLogin())) return true;
        }
        return false;
    }

    @Override
    public boolean isEmailExists(final String email) {
        for (final User user : models) {
            if (email.equals(user.getEmail())) return true;
        }
        return false;
    }

}
