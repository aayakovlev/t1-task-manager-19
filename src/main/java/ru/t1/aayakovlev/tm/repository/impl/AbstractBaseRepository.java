package ru.t1.aayakovlev.tm.repository.impl;

import ru.t1.aayakovlev.tm.exception.field.AbstractFieldException;
import ru.t1.aayakovlev.tm.model.AbstractModel;
import ru.t1.aayakovlev.tm.repository.BaseRepository;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractBaseRepository<M extends AbstractModel> implements BaseRepository<M> {

    protected List<M> models = new ArrayList<>();

    @Override
    public List<M> findAll() {
        return models;
    }

    @Override
    public M findById(final String id) throws AbstractFieldException {
        for (final M model : models) {
            if (model.getId().equals(id)) return model;
        }
        return null;
    }

    @Override
    public M remove(final M model) {
        if (model == null) return null;
        models.remove(model);
        return model;
    }

    @Override
    public M removeById(final String id) throws AbstractFieldException {
        final M model = findById(id);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public M save(final M model) {
        models.add(model);
        return model;
    }

}
