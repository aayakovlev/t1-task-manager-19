package ru.t1.aayakovlev.tm.repository.impl;

import ru.t1.aayakovlev.tm.model.AbstractExtendedModel;
import ru.t1.aayakovlev.tm.repository.ExtendedRepository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractExtendedRepository<M extends AbstractExtendedModel> extends AbstractBaseRepository<M>
        implements ExtendedRepository<M> {

    @Override
    public int count() {
        return models.size();
    }

    @Override
    public void deleteAll() {
        models.clear();
    }

    @Override
    public boolean existsById(final String id) {
        for (final M model : models) {
            if (model.getId().equals(id)) return true;
        }
        return false;
    }

    @Override
    public List<M> findAll(final Comparator<M> comparator) {
        final List<M> sortedModels = new ArrayList<>(models);
        sortedModels.sort(comparator);
        return sortedModels;
    }

    @Override
    public M findByIndex(final Integer index) {
        return models.get(index);
    }

    @Override
    public M removeByIndex(final Integer index) {
        final M model = findByIndex(index);
        if (model == null) return null;
        return remove(model);
    }

}
