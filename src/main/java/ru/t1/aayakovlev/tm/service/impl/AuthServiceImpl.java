package ru.t1.aayakovlev.tm.service.impl;

import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.auth.PasswordIncorrectException;
import ru.t1.aayakovlev.tm.exception.auth.UserNotLoggedException;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.EmailEmptyException;
import ru.t1.aayakovlev.tm.exception.field.LoginEmptyException;
import ru.t1.aayakovlev.tm.model.User;
import ru.t1.aayakovlev.tm.service.AuthService;
import ru.t1.aayakovlev.tm.service.UserService;
import ru.t1.aayakovlev.tm.util.HashUtil;

public final class AuthServiceImpl implements AuthService {

    private final UserService userService;

    private String userId;

    public AuthServiceImpl(UserService userService) {
        this.userService = userService;
    }

    @Override
    public User registry(final String login, final String password, final String email) throws AbstractException {
        return userService.create(login, password, email);
    }

    @Override
    public void login(final String login, final String password) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new EmailEmptyException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new EntityNotFoundException();
        final String hash = HashUtil.salt(password);
        if (!hash.equals(user.getPasswordHash())) throw new PasswordIncorrectException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    public String getUserId() throws UserNotLoggedException {
        if (!isAuth()) throw new UserNotLoggedException();
        return userId;
    }

    @Override
    public User getUser() throws AbstractException {
        if (!isAuth()) throw new UserNotLoggedException();
        final User user = userService.findById(userId);
        if (user == null) throw new EntityNotFoundException();
        return user;
    }

}
