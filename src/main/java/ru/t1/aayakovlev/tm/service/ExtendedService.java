package ru.t1.aayakovlev.tm.service;

import ru.t1.aayakovlev.tm.enumerated.Sort;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.model.AbstractExtendedModel;
import ru.t1.aayakovlev.tm.repository.ExtendedRepository;

import java.util.List;

public interface ExtendedService<M extends AbstractExtendedModel> extends BaseService<M>, ExtendedRepository<M> {

    M changeStatusById(final String id, final Status status) throws AbstractException;

    M changeStatusByIndex(final Integer index, final Status status) throws AbstractException;

    List<M> findAll(final Sort sort);

    M updateById(final String id, final String name, final String description) throws AbstractException;

    M updateByIndex(final Integer index, final String name, final String description) throws AbstractException;

}
