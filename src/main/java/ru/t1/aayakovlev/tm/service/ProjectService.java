package ru.t1.aayakovlev.tm.service;

import ru.t1.aayakovlev.tm.exception.field.AbstractFieldException;
import ru.t1.aayakovlev.tm.model.Project;

public interface ProjectService extends ExtendedService<Project> {

    Project create(final String name) throws AbstractFieldException;

    Project create(final String name, final String description) throws AbstractFieldException;

}
