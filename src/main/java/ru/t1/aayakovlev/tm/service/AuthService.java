package ru.t1.aayakovlev.tm.service;

import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.auth.UserNotLoggedException;
import ru.t1.aayakovlev.tm.model.User;

public interface AuthService {

    User registry(final String login, final String password, final String email) throws AbstractException;

    void login(final String login, final String password) throws AbstractException;

    void logout();

    boolean isAuth();

    String getUserId() throws UserNotLoggedException;

    User getUser() throws AbstractException;

}
