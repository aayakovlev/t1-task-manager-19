package ru.t1.aayakovlev.tm.service.impl;

import ru.t1.aayakovlev.tm.exception.field.AbstractFieldException;
import ru.t1.aayakovlev.tm.exception.field.DescriptionEmptyException;
import ru.t1.aayakovlev.tm.exception.field.NameEmptyException;
import ru.t1.aayakovlev.tm.model.Project;
import ru.t1.aayakovlev.tm.repository.ProjectRepository;
import ru.t1.aayakovlev.tm.service.ProjectService;


public final class ProjectServiceImpl extends AbstractExtendedService<Project, ProjectRepository> implements ProjectService {

    public ProjectServiceImpl(ProjectRepository repository) {
        super(repository);
    }

    @Override
    public Project create(final String name) throws AbstractFieldException {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return repository.create(name);
    }

    @Override
    public Project create(final String name, final String description) throws AbstractFieldException {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return repository.create(name, description);
    }

}
