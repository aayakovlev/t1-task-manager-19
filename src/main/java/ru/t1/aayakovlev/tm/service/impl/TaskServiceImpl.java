package ru.t1.aayakovlev.tm.service.impl;

import ru.t1.aayakovlev.tm.exception.field.AbstractFieldException;
import ru.t1.aayakovlev.tm.exception.field.DescriptionEmptyException;
import ru.t1.aayakovlev.tm.exception.field.IdEmptyException;
import ru.t1.aayakovlev.tm.exception.field.NameEmptyException;
import ru.t1.aayakovlev.tm.model.Task;
import ru.t1.aayakovlev.tm.repository.TaskRepository;
import ru.t1.aayakovlev.tm.service.TaskService;

import java.util.List;

public final class TaskServiceImpl extends AbstractExtendedService<Task, TaskRepository> implements TaskService {

    public TaskServiceImpl(final TaskRepository repository) {
        super(repository);
    }

    @Override
    public Task create(final String name) throws AbstractFieldException {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return repository.create(name);
    }

    @Override
    public Task create(final String name, final String description) throws AbstractFieldException {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return repository.create(name, description);
    }

    @Override
    public List<Task> findByProjectId(final String projectId) throws AbstractFieldException {
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        return repository.findAllByProjectId(projectId);
    }

}
