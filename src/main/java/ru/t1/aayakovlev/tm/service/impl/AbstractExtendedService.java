package ru.t1.aayakovlev.tm.service.impl;

import ru.t1.aayakovlev.tm.enumerated.Sort;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.*;
import ru.t1.aayakovlev.tm.model.AbstractExtendedModel;
import ru.t1.aayakovlev.tm.repository.ExtendedRepository;
import ru.t1.aayakovlev.tm.service.ExtendedService;

import java.util.Comparator;
import java.util.List;

import static ru.t1.aayakovlev.tm.constant.ApplicationConstant.FIRST_ARRAY_ELEMENT_INDEX;

public abstract class AbstractExtendedService<M extends AbstractExtendedModel, R extends ExtendedRepository<M>>
        extends AbstractBaseService<M, R> implements ExtendedService<M> {

    public AbstractExtendedService(R repository) {
        super(repository);
    }

    @Override
    public M changeStatusById(final String id, final Status status) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        final M model = findById(id);
        if (model == null) throw new EntityNotFoundException();
        model.setStatus(status);
        return model;
    }

    @Override
    public M changeStatusByIndex(final Integer index, final Status status) throws AbstractException {
        final int recordCount = repository.count();
        if (index == null || index < FIRST_ARRAY_ELEMENT_INDEX || index >= recordCount)
            throw new IndexIncorrectException();
        if (status == null) throw new StatusEmptyException();
        final M model = findByIndex(index);
        if (model == null) throw new EntityNotFoundException();
        model.setStatus(status);
        return model;
    }

    @Override
    public int count() {
        return repository.count();
    }

    @Override
    public void deleteAll() throws AbstractException {
        repository.deleteAll();
    }

    @Override
    public boolean existsById(final String id) {
        return repository.existsById(id);
    }

    @Override
    public List<M> findAll(final Comparator<M> comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Override
    public List<M> findAll(final Sort sort) {
        if (sort == null) return findAll();
        return findAll(sort.getComparator());
    }

    @Override
    public M findByIndex(final Integer index) throws IndexIncorrectException {
        final int recordsCount = repository.count();
        if (index == null || index < FIRST_ARRAY_ELEMENT_INDEX || index >= recordsCount)
            throw new IndexIncorrectException();
        return repository.findByIndex(index);
    }

    @Override
    public M removeByIndex(final Integer index) throws AbstractFieldException {
        final int recordsCount = repository.count();
        if (index == null || index < FIRST_ARRAY_ELEMENT_INDEX || index >= recordsCount)
            throw new IndexIncorrectException();
        return repository.removeByIndex(index);
    }

    @Override
    public M updateById(final String id, final String name, final String description) throws AbstractException {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        final M model = findById(id);
        if (model == null) throw new EntityNotFoundException();
        model.setName(name);
        model.setDescription(description);
        return model;
    }

    @Override
    public M updateByIndex(final Integer index, final String name, final String description) throws AbstractException {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        final M model = findByIndex(index);
        if (model == null) throw new EntityNotFoundException();
        model.setName(name);
        model.setDescription(description);
        return model;
    }

}
