package ru.t1.aayakovlev.tm.command.project;

import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

public final class ProjectCompeteByIdCommand extends AbstractProjectCommand {

    public static final String DESCRIPTION = "Complete project by id.";

    public static final String NAME = "project-complete-by-id";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.print("Enter id: ");
        final String id = nextLine();
        getProjectService().changeStatusById(id, Status.COMPLETED);
    }

}
