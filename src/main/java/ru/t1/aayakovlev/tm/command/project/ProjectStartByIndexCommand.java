package ru.t1.aayakovlev.tm.command.project;

import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextNumber;

public final class ProjectStartByIndexCommand extends AbstractProjectCommand {

    public static final String DESCRIPTION = "Start project by index.";

    public static final String NAME = "project-start-by-index";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.print("Enter index: ");
        final Integer index = nextNumber() - 1;
        getProjectService().changeStatusByIndex(index, Status.IN_PROGRESS);
    }

}
