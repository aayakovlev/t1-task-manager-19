package ru.t1.aayakovlev.tm.command.project;

import ru.t1.aayakovlev.tm.exception.AbstractException;

public final class ProjectClearCommand extends AbstractProjectCommand {

    public static final String DESCRIPTION = "Remove all projects.";

    public static final String NAME = "project-clear";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[CLEAR PROJECTS]");
        getProjectService().deleteAll();
    }

}
