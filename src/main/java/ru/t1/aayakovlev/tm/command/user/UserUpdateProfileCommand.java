package ru.t1.aayakovlev.tm.command.user;

import ru.t1.aayakovlev.tm.exception.AbstractException;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    public static final String DESCRIPTION = "Update user profile.";

    public static final String NAME = "user-update-profile";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[USER UPDATE PROFILE]");
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.print("Enter first name: ");
        final String firstName = nextLine();
        System.out.print("Enter last name: ");
        final String lastName = nextLine();
        System.out.print("Enter middle name: ");
        final String middleName = nextLine();
        getUserService().updateUser(userId, firstName, lastName, middleName);
    }

}
