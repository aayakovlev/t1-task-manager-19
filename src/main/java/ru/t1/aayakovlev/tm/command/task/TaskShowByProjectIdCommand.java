package ru.t1.aayakovlev.tm.command.task;

import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.model.Task;

import java.util.List;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

public final class TaskShowByProjectIdCommand extends AbstractTaskCommand {

    public static final String DESCRIPTION = "Show tasks by project id.";

    public static final String NAME = "task-show-by-project-id";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SHOW TASKS BY PROJECT ID]");
        System.out.print("Enter projectId: ");
        final String projectId = nextLine();
        final List<Task> tasks = getTaskService().findByProjectId(projectId);
        renderTasks(tasks);
    }

}
