package ru.t1.aayakovlev.tm.command.user;

import ru.t1.aayakovlev.tm.command.AbstractCommand;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.model.User;
import ru.t1.aayakovlev.tm.service.UserService;

public abstract class AbstractUserCommand extends AbstractCommand {

    protected UserService getUserService() {
        return serviceLocator.getUserService();
    }

    @Override
    public String getArgument() {
        return null;
    }

    protected void showUser(final User user) throws EntityNotFoundException {
        if (user == null) throw new EntityNotFoundException();
        System.out.println("Id: " + user.getId());
        System.out.println("Login: " + user.getLogin());
        System.out.println("Email: " + user.getEmail());
    }

}
