package ru.t1.aayakovlev.tm.command.task;

import ru.t1.aayakovlev.tm.exception.AbstractException;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

public final class TaskBindToProjectCommand extends AbstractTaskCommand {

    public static final String DESCRIPTION = "Bind task to project.";

    public static final String NAME = "task-bind-to-project";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.print("Enter project id: ");
        final String projectId = nextLine();
        System.out.print("Enter task id: ");
        final String taskId = nextLine();
        getProjectTaskService().bindTaskToProject(projectId, taskId);
    }

}
