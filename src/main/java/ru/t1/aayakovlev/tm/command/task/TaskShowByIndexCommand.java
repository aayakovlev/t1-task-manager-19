package ru.t1.aayakovlev.tm.command.task;

import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.model.Task;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextNumber;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    public static final String DESCRIPTION = "Show task by index.";

    public static final String NAME = "task-show-by-index";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.print("Enter index: ");
        final Integer index = nextNumber() - 1;
        final Task task = getTaskService().findByIndex(index);
        showTask(task);
    }

}
