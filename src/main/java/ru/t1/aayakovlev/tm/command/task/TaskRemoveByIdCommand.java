package ru.t1.aayakovlev.tm.command.task;

import ru.t1.aayakovlev.tm.exception.AbstractException;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

public final class TaskRemoveByIdCommand extends AbstractTaskCommand {

    public static final String DESCRIPTION = "Task remove by id.";

    public static final String NAME = "task-remove-by-id";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.print("Enter id: ");
        final String id = nextLine();
        getTaskService().findById(id);
        getTaskService().removeById(id);
    }

}
