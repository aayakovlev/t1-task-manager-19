package ru.t1.aayakovlev.tm.command.task;

import ru.t1.aayakovlev.tm.command.AbstractCommand;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.model.Task;
import ru.t1.aayakovlev.tm.service.ProjectTaskService;
import ru.t1.aayakovlev.tm.service.TaskService;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected ProjectTaskService getProjectTaskService() {
        return serviceLocator.getProjectTaskService();
    }

    protected TaskService getTaskService() {
        return serviceLocator.getTaskService();
    }

    @Override
    public String getArgument() {
        return null;
    }

    protected void renderTasks(final List<Task> tasks) {
        int index = 1;
        for (final Task task : tasks) {
            if (task == null) continue;
            System.out.println(index++ + ". " + task);
        }
    }

    protected void showTask(final Task task) {
        if (task == null) return;
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("Project id: " + task.getProjectId());
        System.out.println("Status: " + Status.toName(task.getStatus()));
    }

}
